Pod::Spec.new do |s|
  s.name             = 'Nama'
  s.version          = '1.0.1'
  s.summary          = 'faceunity-nama'
  s.description      = <<-DESC
https://github.com/Faceunity/FUP2AArt 中的库
                       DESC
  s.homepage         = 'https://gitee.com/ext-cocoapods/faceunity-nama'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :git => 'https://gitee.com/ext-cocoapods/faceunity-nama.git', :tag => s.version.to_s }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'

  s.platform = :ios, '9.0'
  s.static_framework = true
  s.vendored_libraries = 'libCNamaSDK.a', 'libfuai.a'
  s.resources = ['**/*.{bundle,txt}']
  s.frameworks = 'OpenGLES','Accelerate','CoreMedia','AVFoundation','CoreML'
  s.libraries = 'stdc++'

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { "EXCLUDED_ARCHS[sdk=iphonesimulator*]" => "arm64", "EXCLUDED_ARCHS[sdk=iphonesimulator*]" => "arm64" }
end
